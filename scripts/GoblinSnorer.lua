local dev,dev_spawn,dev_z4,dev_convert_all,dev_invincible
-- dev=true
-- dev_spawn=dev
dev_z4=dev
-- dev_invincible=dev
-- dev_convert_all=dev

local action = require "necro.game.system.Action"
local attack = require "necro.game.character.Attack"
local commonSpell = require "necro.game.data.spell.CommonSpell"
local components = require "necro.game.data.Components"
local confusion = require "necro.game.character.Confusion"
local currentLevel = require "necro.game.level.CurrentLevel"
local customEntities = require "necro.game.data.CustomEntities"
local damage = require "necro.game.system.Damage"
local ecs = require "system.game.Entities"
local event = require "necro.event.Event"
local invincibility = require "necro.game.character.Invincibility"
local move = require "necro.game.system.Move"
local object = require "necro.game.object.Object"
local player = require "necro.game.character.Player"
local spell = require "necro.game.spell.Spell"
local spellItem = require "necro.game.item.SpellItem"
local voice = require "necro.audio.Voice"

local enemyPool = require "enemypool.EnemyPool"

components.register{
  marker={},
}

if dev then
  event.levelLoad.add("spawn", {order="entities"},function (ev)
    if dev_spawn then
      object.spawn("goblinsnorer_enemy",-1,-1)
      -- object.spawn("SleepingGoblin",1,-1)
    end

    object.spawn("FoodMagic4",0,0)

    if dev_invincible then
      for _,p in ipairs(player.getPlayerEntities()) do
        invincibility.activate(p,999)
      end
    end
  end)
end
if dev_z4 then
  event.levelGenerate.add("generateProceduralLevel", {sequence=-1}, function (ev)
    -- all zones are zone 4
    local zone = 4
    ev.level = (ev.level%4) + (zone-1)*4
  end)
end

-- yippee
event.objectDig.add("registerGoblinsnorer", {order="spell", filter="goblinsnorer_marker"}, function(ev)
  spell.cast(ev.entity,"SpellcastEarthquake",0)
end)

event.levelLoad.add("registerGoblinsnorer", {order="entities"}, function(ev)
  local floor=currentLevel.getFloor()
  local probability=floor==1 and 0
    or floor==2 and 0.05
    or floor==3 and 0.20
    or 0
  if dev_convert_all then probability=1 end
  enemyPool.registerConversion{
    from="SleepingGoblin",
    to="goblinsnorer_enemy",
    probability=probability,
  }
end)

customEntities.extend {
  name="enemy",
  template=customEntities.template.enemy("sleeping_goblin"),
  components={
    goblinsnorer_marker={},
    friendlyName={name="Goblin Snorer"},
    beatDelay={interval=3},
    sprite={
      texture="mods/goblinsnorer/images/goblin-snorer.png",
    },
  },
}
